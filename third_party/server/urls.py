"""third_party URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from . import views
urlpatterns = [
    path('set_public_key/<str:login>', views.set_public_key),
    path('get_public_key/<str:login>', views.get_public_key),

    path('set_paraphe/<str:login>/<str:paraphe_name>', views.set_paraphe),
    path('set_session_key/<str:login>', views.set_session_key),

    path('create_event/<str:event_name>', views.create_event),
    path('get_session_hash/<str:event_name>/<str:login>', views.get_session_hash),
    path('send_sign/<str:event_name>/<str:login>', views.send_sign),

    path('generate_pdf/<str:event_name>/<str:resoponsable>', views.generate_pdf)
]
