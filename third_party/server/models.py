from django.db import models


class Paraphe(models.Model):
    login = models.CharField(max_length=255, primary_key=True)
    has_paraphe = models.BooleanField(default=False)
    paraphe_name = models.CharField(max_length=512, null=True)
    has_session_key = models.BooleanField(default=False)

class Archive(models.Model):
    name = models.CharField(max_length=255)
    hash = models.CharField(max_length=1024)
