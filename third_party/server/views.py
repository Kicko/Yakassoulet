from django.http import HttpResponse, FileResponse
from .models import *
import json, os, hashlib, gnupg, zipfile, shutil
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch
from reportlab.lib.pagesizes import A4
from reportlab.platypus import Image
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def set_public_key(request, login):
    if request.method != 'POST':
        return HttpResponse(400)
    print("login: "+ login + "\npublic key:\n" + request.body.decode('utf-8'))
    with open("server/keys/" + login + ".pub", "w") as key:
        key.write(request.body.decode('utf-8'))

    Paraphe(login=login).save()

    return HttpResponse(200)


def get_public_key(request, login):
    par = Paraphe.objects.get(login=login)
    if par is None:
        return HttpResponse(400)

    with open(par.public_key, "r") as key:
        response = FileResponse(key)

    return response


@csrf_exempt
def set_paraphe(request, login, paraphe_name):
    if request.method != 'POST':
        return HttpResponse(400)

    par = Paraphe.objects.get(login=login)
    if par is None:
        return HttpResponse("Not valid request", 400)
    par.has_paraphe = True
    par.paraphe_name = paraphe_name
    par.save()

    path = "paraphes/" + paraphe_name
    with open(path, "w") as paraphe:
        paraphe.write(request.body)

    return HttpResponse(status=200)


@csrf_exempt
def set_session_key(request, login):
    if request.method != 'POST':
        return HttpResponse(400)

    par = Paraphe.objects.get(login=login)
    if par is None:
        return HttpResponse("Not valid request", 400)
    par.has_session_key = True
    par.save()

    with open("keys/" + login + ".sk") as session_key:
        session_key.write(request.body)

    return HttpResponse(200)


@csrf_exempt
def create_event(request, event_name):
    if request.method != 'POST':
        return HttpResponse(400)

    os.makedirs(event_name)

    with open(event_name + "/info.json", 'w') as info:
        info.write(request.body.decode('utf-8'))
        print("writing " + request.body.decode('utf-8'))

    shutil.make_archive("server/archives/" + event_name, 'zip', base_dir=event_name)

    shutil.rmtree(event_name)

    hasher = hashlib.md5()
    with open("server/archives/" + event_name + ".zip", "rb") as archive:
        arc = archive.read()
        hasher.update(arc)
        hash = hasher.hexdigest()
        print("hash: " + hash)

    Archive(name=event_name, hash=hash).save()
    return HttpResponse(200)


def update_archive(name, file_path):
    arc = Archive.objects.get(name=name)

    zf = zipfile.ZipFile("server/archives/" + name + ".zip", "w")
    zf.write(file_path)
    zf.close()

    with open("server/archives/" + name + ".zip") as archive:
        arc.hash = hashlib.sha1(archive.read())


def get_session_hash(request, event_name, login):
    arc = Archive.objects.get(event_name)
    par = Paraphe.objects.get(login)
    if arc is None or par is None:
        return HttpResponse(400)

    #TODO refacto ça fdp
    session_hash = {'session': par.session_key,
                    'hash': arc.hash}

    return HttpResponse(session_hash, content_type="application/json")


@csrf_exempt
def send_sign(request, event_name, login):
    if request.method != 'POST':
        return HttpResponse(400)

    gpg = gnupg.GPG()  # probable error

    verified = gpg.verify_data("server/keys/" + login + ".pub", request.body)
    if not verified:
        return HttpResponse("signature does not match key", 400)

    path = event_name + login + "_tmp"

    os.mkdir(path)

    with open(path + ".zip") as archive:
        archive.write(request.body)

    zf = zipfile.ZipFile(path + ".zip")
    zf.extractall(path)
    zf.close()

    arch = Archive.objects.get(name=event_name)

    with open(path + "/hash") as hashfile:
        if hashfile.read() != arch.hash:
            return HttpResponse("bad hash", 400)

    sesskey = b""
    with open(path + "/sesskey") as sesskey_file:
        sesskey = sesskey_file.read()

    gpg.import_keys("keys/" + login + ".sk")
    user = Paraphe.objects.get(login=login)
    with open(path + "/" + user.paraphe_name) as paraphe:
        with open(user.paraphe) as encrypted_paraphe:
            decr = gpg.decrypt(encrypted_paraphe)
            if not decr.ok:
                return HttpResponse("Could not decrypt paraphe with session key", 400)
            paraphe.write(gpg.decrypt(encrypted_paraphe)) # TODO A FIX CA MARCHE PAS supposement decode le paraphe avec la session key

    update_archive(event_name, path + "/paraphe")

    shutil.rmtree(path)

    return HttpResponse(200)


def generate_pdf(request, event_name, responsable_login):

    if Archive.objects.filter(name=event_name).count() != 1:
        return HttpResponse()

    send_sign(request, event_name, responsable_login) #Update the archive with respo's signature

    zf = zipfile.ZipFile("server/archives/" + event_name + ".zip")
    zf.extractall(event_name + "_tmp")
    zf.close()

    info = None
    with open(event_name + "_tmp/info.json") as json_file:
        info = json.load(json_file.read())

    association = info['association']
    respo = info['respo']
    tutor = info['tutor']

    pdf_name = "fiche-event-" + info['event_id'] + "-" + event_name + ".pdf"

    # create a canvas (a pdf)
    p = canvas.Canvas(event_name + "_tmp/" + pdf_name)

    # margin of the page
    left_margin = inch
    right_margin = A4[0] - inch

    p.setFont("Times-Bold", 30)
    # add text at center (first arg = padding left, 2nd arg = padding down)
    p.drawCentredString(0.5 * A4[0], 10.5 * inch, "Fiche Event " + info['event_id'])

    p.setFont("Times-Bold", 12)

    # create a paragraph
    tx = p.beginText(left_margin, 9.5 * inch)
    # add line to paragraphe
    tx.textLine("organisé par l'association : " + association)

    tx.textLine("Nom de l'Event : " + event_name)

    tx.textLine("Date de l'Event : " + info['date'])
    tx.textLine("Horaire de début : " + info['hour_begin'])
    tx.textLine("Horaire de fin : " + info['event.hour_end'])


    tx.textLine("Recurrence :")
    if (info['recurrence']):
        tx.textLine("oui")
        tx.textLine("Frequence : " + info['frequence'])
        tx.textLine("Fin : " + info['frequence_end'])
    else:
        tx.textLine("non")

    tx.textLine("Responsable sur place : " + respo['name'])
    tx.textLine("Tel du responsable sur place : " + respo['telephone'])
    tx.textLine("Classe du responsable sur place : " + respo['class'])  #HAS BEEN MODIFIED HERE, respo_class to respo object

    if (info['alcoholic_drinks']):
        tx.textLine("Tuteur : " + tutor['name'])
        tx.textLine("Tel du tuteur : " + tutor['telephone'])
        tx.textLine("Poste du tuteur : " + tutor['role']) #HAS BEEN MODIFIED HERE TOO

    tx.textLine("Nombre d'externes attendus : " + info['nb_externs'])
    tx.textLine("Nombre d'etudiants IONIS attendus : " + info['nb_students'])
    tx.textLine("Nombre de membres de l'association : " + info['nb_members'])

    tx.textLine("Description de l'event :")
    tx.textLine(info['description'])

    tx.textLine("Assurance :")
    if (info['insurance']):
        tx.textLine("oui")
    else:
        tx.textLine("non")

    tx.textLine("Materiel : " + info['material'])

    tx.textLine("Boissons alcoolisees :")
    if (info['alcoholic_drinks']):
        tx.textLine("oui")
    else:
        tx.textLine("non")

    tx.textLine("Site : " + info['site'])
    tx.textLine("Salles prévues :")
    rs = ""
    k = 0
    # return carriage if > 85 characters
    tx.textLine(info['reserved_rooms'])

    tx.textLine("Materiel présent:")
    tx.textLine(info['material'])

    tx.textLine("Boissons prévues :")
    if not info['soft_drinks'] and not info['alcoholic_drinks']:
        tx.textLine("Aucune boisson")
    elif info['soft_drinks']:
        tx.textLine("Boissons non alcoolisées")
    else:
        tx.textLine("Boissons non alcoolisées et alcoolisées")

    tx.textLine("Commentaires : " + info['comments'])

    # put paragraph to page
    p.drawText(tx)

    # rectangles and lines : design of pdf
    p.rect(60, 580, 480, 100)
    p.line(60, 665, 540, 665)

    # respo asso
    p.drawString(right_margin - inch, 2.2 * inch, "Responsable associations")
    # RESPO ASSO
    image = Image(event_name + "_tmp/" + Paraphe.objects.get(login=responsable_login).paraphe_name)
    image._restrictSize(2 * inch, 1.5 * inch)
    image.drawOn(p, right_margin - inch, 0.5 * inch)

    # TUTOR
    p.drawString(right_margin - 2.5 * inch, 2.2 * inch, "Tuteur")
    if info['alcoholic_drinks']:
        image = Image(event_name + "_tmp/" + Paraphe.objects.get(login=tutor['name']).paraphe_name)
        image._restrictSize(2 * inch, 1.5 * inch)
        image.drawOn(p, right_margin - 3 * inch, 0.5 * inch)

    # president of asso
    p.drawString(right_margin - 5 * inch, 2.2 * inch, "Président de l'association")
    image = Image(event_name + "_tmp/" + Paraphe.objects.get(login=info['president']).paraphe_name)
    image._restrictSize(2 * inch, 1.5 * inch)
    image.drawOn(p, right_margin - 5 * inch, 0.5 * inch)

    # respo of event
    p.drawString(right_margin - 7 * inch, 2.2 * inch, "Responsable sur place")
    image = Image(event_name + "_tmp/" + Paraphe.objects.get(login=respo['name']).paraphe_name)
    image._restrictSize(2 * inch, 1.5 * inch)
    image.drawOn(p, right_margin - 7 * inch, 0.5 * inch)

    # save the pdf
    p.showPage()
    p.save()

    try:
        path = event_name + "_tmp/" + pdf_name

        response = None
        with open(path, 'rb') as pdf:
            response = FileResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'inline; filename=' + pdf_name + '.pdf'
        return response
    except:
        raise HttpResponse(400)

