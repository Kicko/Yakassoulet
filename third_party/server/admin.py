from django.contrib import admin

# Register your models here.
from . import models

@admin.register(models.Paraphe)
class ParapheAdmin(admin.ModelAdmin):
    pass

@admin.register(models.Archive)
class ArchiveAdmin(admin.ModelAdmin):
    pass
