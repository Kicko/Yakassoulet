# Yakassoulet
Yaka*

# Start the project
cd Yakassoulet
pipenv install Pipfile                  # it will install django

# Run the server
pipenv shell
python manage.py makemigrations events
python manage.py migrate
python manage.py runserver

# Create an app
python manage.py startapp <name>        # don't forget to link it in urls.py

# See the database
sqlite3 db.sqlite3
.schema

# Create an admin
python manage.py createsuperuser        # 127.0.0.1:8000/admin

# Run tests
python manage.py test events

# Init database
python manage.py loaddata init_salle.json


(Tout le code est dans events/)

site/
  accueil                               started
  events/
    list of events
    form to create an event             started
  associations/
    list of associations                started
    page perso of asso
    create
    modify
    delete an association
  profil/
    list of names                       started
    page perso of people
  inscrip/connexion                     started




To allow sending email from google account
https://myaccount.google.com/lesssecureapps?pli=1
