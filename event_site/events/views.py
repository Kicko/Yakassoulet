from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.template import loader
from django.contrib.auth.decorators import login_required
import os, json, traceback
from django.utils import timezone
import gnupg, requests
from event_site.settings import THIRD_PARTY_ADDRESS

import ics

from . import forms

from django.views import generic
from .models import *

from datetime import datetime

# rajout/changed : test pour l'accueil
@login_required
def accueil(request):
    template_name = 'home.html'
    person = Person.objects.get(name=request.user.username)
    context = {'is_administration': Administration.objects.filter(person_id=person).count(),
                'user': person}
    return render(request, template_name, context)


# simple pages
def index(request):
    template_name = 'home_reduced.html'
    if request.user.is_authenticated:
        return accueil(request)
    else:
        context= {'is_connected': request.user.is_authenticated}
        return render(request, template_name, context)

#def asso(request):
#    return render(request, 'association.html')

# list of associations
@login_required
def AssosView(request):
    template_name = 'new_asso.html'      # html to load
    context_object_name = 'associations'     # name given to the list (for the html)
    person = Person.objects.get(name=request.user.username)
    context = { 'is_respo_asso': person.name ==
                                 Administration.objects.get(role="Respo asso").person_id.name,
                'associations': Association.objects.all(),
                'is_administration': Administration.objects.filter(person_id=person).count(),
                'user': person
                }

    return render(request, template_name, context)


@login_required
def AssoDetail(request, name):
        person = Person.objects.get(name=request.user.username)
        asso = get_object_or_404(Association, name=name)
        events = []
        for e in Event.objects.filter(association_id=asso):
            events.append(e)
        for a in AssoSupEvent.objects.filter(association_id=asso):
            events.append(a.event_id)
        context = { 'asso': asso,
            'president': asso.bureau.get(role="President").person_id,
            'can_add_del': asso.bureau.filter(person_id=person).count(),
            'is_respo_asso': person.name ==
                             Administration.objects.get(role="Respo asso").person_id.name,
            'is_administration': Administration.objects.filter(person_id=person).count(),
            'events': events,
            'member_list': Member.objects.filter(association_id=asso),
            'user': person,
            'description': asso.description.split("\n"),
        }
        return render(request, 'new_asso_detail.html', context)



# form (example)
def get_name(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = forms.NameForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect('/thanks')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = forms.NameForm()

    return render(request, 'name.html', {'form': form})

# redirection page
def thanks(request):
    return render(request, 'thanks.html')

# redirection page
def not_authorized(request):
    return render(request, 'not_authorized.html')

# events page
#def events(request):
#    return render(request, 'event_page.html')

def create_ics():
    c = ics.Calendar()

    for event in Event.objects.filter(respo_asso_sign=True):
        e = ics.Event()

        e.name = event.name
        e.begin = '20140101 00:00:00'
        e.end = '20140101 00:00:00'
        e.decription = event.description
        e.location = 'KB'
        c.events.add(e)

    with open('events/static/events.ics', 'w') as f:
        f.writelines(c)

# list of events
@login_required
def EventsView(request):
    person = Person.objects.get(name=request.user.username)
    create_ics()

    events = []

    for event in Event.objects.all():
        if event.respo_sign and event.president_sign and event.tutor_sign \
            and event.respo_asso_sign:
            events.append(event)

    context = {
        'events': events,
        'is_administration': Administration.objects.filter(person_id=person).count(),
        'user': person,
        }

    return render(request, 'new_event_page.html', context)

# list of profils
@login_required
def profils_view(request):
    if Person.objects.filter(name=request.user.username).count() != 1:
        raise Http404("You can't see the profils if you are not connected")

    person = Person.objects.get(name=request.user.username)
    profils = []
    if person.administration == True:
        profils = Person.objects.all().order_by('name')
    else:
        profils = Person.objects.filter(administration=False).order_by('name')
    context = { 'profils': profils,
                'is_administration': Administration.objects.filter(person_id=person).count(),
                'user': person,
            }
    return render(request, 'profils.html', context)


@login_required
def profil_detail(request, name_):
    profil = get_object_or_404(Person, name=name_)
    person = Person.objects.get(name=request.user.username)
    respo_asso = False
    if person == Administration.objects.get(role="Respo asso").person_id:
        respo_asso = True
    own_profile = True
    if profil.name != person.name:
        own_profile = False

    asso_list = []
    for member in Member.objects.filter(person_id=profil):
        if Bureau.objects.filter(person_id=profil, association_id=member.association_id).count() == 1:
            asso_list.append((member.association_id.name, Bureau.objects.get(person_id=profil, association_id=member.association_id).role))
        else:
            asso_list.append((member.association_id.name, "membre"))

    context = { 'profil': profil,
                'events_to_valid': get_to_valid_events(profil.name),
                'is_administration': Administration.objects.filter(person_id=person).count(),
                'asso_list': asso_list,
                'is_respo_asso': respo_asso,
                'is_own_profile': own_profile,
                'user': person,
              }
    # if respo des assos: add premium
    return render(request, 'profil_detail.html', context)


@login_required
# form of fiche event
def get_event(request):
    error = ""

    person = Person.objects.get(name=request.user.username)
    VJ_checked = []
    KB_checked = []

    if request.method == 'POST':
        form = forms.EventForm(request.POST)

        # save brouillon button
        if 'save-brouillon' in request.POST:
            e_asso_name = form.data['association']
            e_respo_name = form.data['respo_name']
            e_tutor_name = form.data['tutor_name']
            e_name = form.data['name']
            e_date = form.data['date']
            e_hour_begin = form.data['hour_begin']
            e_hour_end = form.data['hour_end']
            try:
                e_rec = form.data['recurrence']
            except:
                e_rec = "1"
            e_freq = form.data['frequence']
            e_freq_end = form.data['frequence_end']
            e_respo_class = form.data['respo_class']
            e_nb_s = form.data['nb_students']
            e_nb_ext = form.data['nb_externs']
            e_nb_memb = form.data['nb_members']
            e_desc = form.data['description']
            try:
                e_ins = form.data['insurance']
            except:
                e_ins = "1"
            e_mat = form.data['material']
            try:
                e_alcool = form.data['alcoholic_drinks']
            except:
                e_alcool = "1"
            try:
                e_soft = form.data['soft_drinks']
            except:
                e_soft = "1"
            e_comments = form.data['comments']

            chosen_room_VJ = request.POST.getlist('check_rooms_VJ[]')
            chosen_room_KB = request.POST.getlist('check_rooms_KB[]')

            vj = ",".join(chosen_room_VJ)
            kb = ",".join(chosen_room_KB)

            e = Brouillon(name=e_name, date=e_date, hour_begin=e_hour_begin,
                      hour_end=e_hour_end, rooms_VJ=vj, rooms_KB=kb,
                      frequence=e_freq, frequence_end=e_freq_end,
                      respo_class=e_respo_class, nb_students=e_nb_s,
                      nb_members=e_nb_memb, nb_externs=e_nb_ext,
                      description=e_desc, material=e_mat,
                      comments=e_comments, association=e_asso_name,
                      respo_id=e_respo_name, tutor_id=e_tutor_name,
                      creator_id=person, creation_date=datetime.now()) # to change with the logged person
            if e_rec == "on":
                e.recurrence = True
            if e_ins == "on":
                e.insurance = True
            if e_alcool == "on":
                e.alcoholic_drinks = True
            if e_soft == "on":
                e.soft_drinks = True
            e.save()
            return HttpResponseRedirect('/')



        # submit button
        elif 'submit' in request.POST and form.is_valid():
            # put the new person in the database
            e_asso_name = form.cleaned_data['association']
            e_respo_name = form.cleaned_data['respo_name']
            e_tutor_name = form.cleaned_data['tutor_name']
            # valid asso, respo, tutor ?
            try:
                error = "names are not valid"
                if Association.objects.filter(name=e_asso_name).count() == 1 \
                    and Person.objects.filter(name=e_respo_name).count() == 1 \
                    and (form.cleaned_data['alcoholic_drinks'] == False \
                    or Person.objects.filter(name=e_tutor_name, administration=True).count() == 1):

                    error = "cannot get rooms"
                    chosen_room_VJ = request.POST.getlist('check_rooms_VJ[]')
                    chosen_room_KB = request.POST.getlist('check_rooms_KB[]')


                    error = "info of event"
                    e_asso = Association.objects.get(name=e_asso_name)
                    e_respo = Person.objects.get(name=e_respo_name)
                    e_name = form.cleaned_data['name']
                    e_date = form.cleaned_data['date']
                    e_hour_begin = form.cleaned_data['hour_begin'].strftime("%H:%M:%S")
                    e_hour_end = form.cleaned_data['hour_end'].strftime("%H:%M:%S")
                    e_rec = form.cleaned_data['recurrence']
                    e_freq = form.cleaned_data['frequence']
                    e_freq_end = form.cleaned_data['frequence_end']
                    e_respo_class = form.cleaned_data['respo_class']
                    e_nb_s = form.cleaned_data['nb_students']
                    e_nb_ext = form.cleaned_data['nb_externs']
                    e_nb_memb = form.cleaned_data['nb_members']
                    e_desc = form.cleaned_data['description']
                    e_ins = form.cleaned_data['insurance']
                    e_mat = form.cleaned_data['material']
                    e_alcool = form.cleaned_data['alcoholic_drinks']
                    e_soft = form.cleaned_data['soft_drinks']
                    e_comments = form.cleaned_data['comments']

                    e_tutor_administration = None
                    if e_alcool:
                        e_tutor = Person.objects.get(name=e_tutor_name, administration=True)
                        e_tutor_administration = Administration.objects.get(person_id=e_tutor.id)

                    # check the rooms
                    for room in chosen_room_VJ:
                        r = PossibleRoom.objects.get(KB_VJ=True, name=room)
                        for r2 in ReservedRoom.objects.filter(room_id=r):
                            error = "respo asso sign ?"
                            if not r2.event_id.respo_asso_sign:
                                continue
                            error = "fail compare hour"
                            if r2.event_id.date == e_date and \
                              r2.event_id.hour_begin < e_hour_end and \
                              r2.event_id.hour_end > e_hour_begin:
                                # TODO: check frequence
                                error = "already reserved"
                                raise Exception("already reserevd")

                    for room in chosen_room_KB:
                        r = PossibleRoom.objects.get(KB_VJ=False, name=room)
                        for r2 in ReservedRoom.objects.filter(room_id=r):
                            if not r2.event_id.respo_asso_sign:
                                continue
                            error = "fail compare hour"
                            if r2.event_id.date == e_date and \
                              r2.event_id.hour_begin < e_hour_end and \
                              r2.event_id.hour_end > e_hour_begin:
                                error = "already reserved"
                                raise Exception("already reserevd")

                    error = "fail creating event, try to change the name"
                    e = Event(name=e_name, date=e_date, hour_begin=e_hour_begin,
                              hour_end=e_hour_end, recurrence=e_rec,
                              frequence=e_freq, frequence_end=e_freq_end,
                              respo_class=e_respo_class, nb_students=e_nb_s,
                              nb_members=e_nb_memb, nb_externs=e_nb_ext,
                              description=e_desc, insurance=e_ins, material=e_mat,
                              alcoholic_drinks=e_alcool, soft_drinks=e_soft,
                              comments=e_comments, association_id=e_asso,
                              respo_id=e_respo, tutor_id=e_tutor_administration,
                              creator_id=person) # to change with the logged person

                    error = "fail getting the president"
                    # send an email to the president of association
                    president = Bureau.objects.get(association_id=e_asso, role="President").person_id

                    error = "fail sending emails"
                    send_email_to([president.email], 1)
                    send_email_to([request.user.email], 0)

                    error = "fail saving event"
                    e.save()

                    error = "fail reserving the rooms"
                    for room in chosen_room_VJ:
                        r = PossibleRoom.objects.get(KB_VJ=True, name=room)
                        reserved = ReservedRoom(event_id=e, room_id=r)
                        reserved.save()
                    for room in chosen_room_KB:
                        r = PossibleRoom.objects.get(KB_VJ=False, name=room)
                        reserved = ReservedRoom(event_id=e, room_id=r)
                        reserved.save()

                    rs = ""
                    k = 0
                    # return carriage if > 85 characters
                    for room in ReservedRoom.objects.filter(event_id=e):
                        if k != 0:
                            rs += ", "
                        if k > 85:
                            rs += "\n"
                            k = 0

                        rs += room.room_id.name
                        k += len(room.room_id.name) + 2
                    # create object for trusted third party
                    error = "fail sending fiche"
                    fiche = {
                        'respo': {
                            'name': e_respo_name,
                            'telephone': e_respo.telephone,
                            'class': e_respo_class
                        },
                        'association': e_asso_name,
                        'event_id': e_name,
                        'date': str(e_date),
                        'hour_begin': str(e_hour_begin),
                        'hour_end': str(e_hour_end),
                        'recurrence': e_rec,
                        'frequence': e_freq,
                        'frequence_end': e_freq_end,
                        'alcoholic_drinks': e_alcool,
                        'nb_externs': e_nb_ext,
                        'nb_students': e_nb_s,
                        'nb_members': e_nb_memb,
                        'description': e_desc,
                        'insurance': e_ins,
                        'material': e_mat,
                        'site': "VJ" if len(chosen_room_VJ) > 0 else "KB",
                        'reserved_rooms': rs,
                        'soft_drinks': e_soft,
                        'comment': e_comments,
                    }
                    if e_alcool:
                        fiche['tutor'] = {
                            'name': e_tutor_name,
                            'telephone': e_tutor.telephone,
                            'class': e_tutor_administration.role
                        }


                    # Send data to trusted third party
                    requests.post(THIRD_PARTY_ADDRESS + "/create_event/" + e_name,
                                  data=json.dumps(fiche))
                    # redirect
                    return HttpResponseRedirect('/thanks')
            except Exception:

                print(error)
                print(traceback.format_exc())
                form = forms.EventForm()
        else:
            error = "Form is not valid"
            form = forms.EventForm()

        # get brouillon (with button)
        if 'get-brouillon' in request.POST:
            error = ""

            if Brouillon.objects.filter(creator_id=person).count() == 0:
                error = "No saved brouillon"
                form = forms.EventForm()
            else:
                for b in Brouillon.objects.filter(creator_id=person).order_by('-creation_date')[:1]:
                    form = forms.EventForm(initial={'name': b.name,
                                                    'association': b.association,
                                                    'creator_id': b.creator_id,
                                                    'date': b.date,
                                                    'hour_begin': datetime.strptime(b.hour_begin, "%H:%M:%S"),
                                                    'hour_end': datetime.strptime(b.hour_end, "%H:%M:%S"),
                                                    'recurrence': b.recurrence,
                                                    'frequence': b.frequence,
                                                    'frequence_end': b.frequence_end,
                                                    'respo_name': b.respo_id,
                                                    'respo_class': b.respo_class,
                                                    'tutor_name': b.tutor_id,
                                                    'nb_externs': b.nb_externs,
                                                    'nb_members': b.nb_members,
                                                    'nb_students': b.nb_students,
                                                    'insurance': b.insurance,
                                                    'material': b.material,
                                                    'soft_drinks': b.soft_drinks,
                                                    'alcoholic_drinks': b.alcoholic_drinks,
                                                    'comments': b.comments,
                                                    'description': b.description,
                                          })
                    VJ_checked = b.rooms_VJ.split(",")
                    KB_checked = b.rooms_KB.split(",")
                    b.delete()
        else:
            form = forms.EventForm()

    else:
        form = forms.EventForm()

    users = []
    administ = []
    for e in Person.objects.all():
        users.append(e.name)
        if e.administration:
            administ.append(e.name)
    assos = []
    for a in Association.objects.all():
        assos.append(a.name)

    room_VJ = []
    room_KB = []
    for room in PossibleRoom.objects.all().order_by('name'):
        if room.KB_VJ:
            if room.name in VJ_checked:
                room_VJ.append((room.name, True))
            else:
                room_VJ.append((room.name, False))
        else:
            if room.name in KB_checked:
                room_KB.append((room.name, True))
            else:
                room_KB.append((room.name, False))

    context = {
        'form': form,
        'error': error,
        'association': assos,
        'users': users,
        'administration': administ,
        'rooms': { 'VJ' : room_VJ, 'KB' : room_KB },
        'user': person,
        'is_administration': Administration.objects.filter(person_id=person).count() > 0,
    }
    return render(request, 'fiche_event.html', context)


# get the list of events to valid
def get_to_valid_events(username):
    person = Person.objects.get(name=username)
    fiches_to_sign = []

    for event in Event.objects.all():
        if not event.respo_sign:
            if person == event.respo_id:
                fiches_to_sign.append(event)
        elif not event.president_sign:
            president = event.association_id.bureau.get(role="President").person_id
            if person == president:
                fiches_to_sign.append(event)
        elif not event.tutor_sign:
            if person == event.tutor_id.person_id:
                fiches_to_sign.append(event)
        elif not event.respo_asso_sign:
            if Administration.objects.filter(role="Respo asso").count() == 1:
                if person == Administration.objects.get(role="Respo asso").person_id:
                    fiches_to_sign.append(event)
    return fiches_to_sign

@login_required
def sign(request, event_name):
    person = Person.objects.get(name=request.user.username)

    for event in Event.objects.filter(name=event_name):
        if not event.respo_sign:
            if person == event.respo_id:
                event.respo_sign = True
                event.save()

                send_email_to([person.email], 2)
                president = event.association_id.bureau.get(role="President").person_id
                send_email_to([president.email], 1)

                return HttpResponseRedirect('/thanks')

        elif not event.president_sign:
            president = event.association_id.bureau.get(role="President").person_id
            if person == president:
                event.president_sign = True
                event.save()

                send_email_to([person.email], 2)

                if event.tutor_id == None:
                    event.tutor_sign = True
                    event.save()
                    respo_asso = Administration.objects.get(role="Respo asso")
                    send_email_to([respo_asso.person_id.email], 1)
                    return HttpResponseRedirect('/thanks')

                send_email_to([event.tutor_id.person_id.email], 1)

                return HttpResponseRedirect('/thanks')

        elif not event.tutor_sign:
            if person == event.tutor_id.person_id:
                event.tutor_sign = True
                event.save()

                send_email_to([person.email], 2)
                respo_asso = Administration.objects.get(role="Respo asso")
                send_email_to([respo_asso.person_id.email], 1)

                return HttpResponseRedirect('/thanks')

        elif not event.respo_asso_sign:
            if person == Administration.objects.get(role="Respo asso").person_id:
                event.respo_asso_sign = True
                event.save()

                send_email_to([person.email], 2)
                send_email_to([event.creator_id.email], 3)

                # conflict
                for ev2 in Event.objects.filter(date=event.date, respo_asso_sign=False):
                    if ev2 != event \
                        and ev2.hour_begin <= event.end \
                        and ev2.hour_end >= event.hour_begin:

                        remove_conflict_event(event, ev2)


                return HttpResponseRedirect('/thanks')

    raise Http404("You can't sign this event")


def save_as_brouillon(e):
    chosen_room_VJ = []
    chosen_room_KB = []

    for room in ReservedRoom.objects.filter(event_id=e):
        if room.room_id.KB_VJ:
            chosen_room_VJ.append(room.room_id.name)
        else:
            chosen_room_KB.append(room.room_id.name)


    vj = ",".join(chosen_room_VJ)
    kb = ",".join(chosen_room_KB)

    e_hour_begin = e.hour_begin.strftime("%H:%M:%S")
    e_hour_end = e.hour_end.strftime("%H:%M:%S")

    b = Brouillon(name=e.association_id.name,
              date=e.date,
              hour_begin=e_hour_begin,
              hour_end=e_hour_end,
              rooms_VJ=vj,
              rooms_KB=kb,
              frequence=e.frequence,
              frequence_end=e.frequence_end,
              respo_class=e.respo_class,
              nb_students=e.nb_students,
              nb_members=e.nb_members,
              nb_externs=e.nb_externs,
              description=e.description,
              material=e.material,
              comments=e.comments,
              association=e.association_id.name,
              respo_id=e.respo_id.name,
              creator_id=e.creator_id,
              creation_date=datetime.now()) # to change with the logged person
    if e.tutor_id != None:
        b.tutor_id = e.tutor_id.name

    b.save()


@login_required
def reject(request, event_name):
    person = Person.objects.get(name=request.user.username)

    for event in Event.objects.filter(name=event_name):
        president = event.association_id.bureau.get(role="President").person_id
        respo_asso = Administration.objects.get(role="Respo asso")

        if person == event.respo_id or person == president \
            or person == event.tutor_id.person_id \
            or person == respo_asso:

            send_email_to([event.creator_id.email], -1)

            save_as_brouillon(event)

            event.delete()
            return HttpResponseRedirect('/thanks')

    raise Http404("You can't reject this event")

def remove_conflict_event(event_premium, event):
    for room in ReservedRoom.objects.filter(event_id=event_premium):
        for r in ReservedRoom.objects.filter(event_id=event, room_id=room.room_id):
            send_email_to([event.creator_id.email], -2)
            event.delete()
            return

@login_required
def premium(request, event_name):
    person = Person.objects.get(name=request.user.username)

    for event in Event.objects.filter(name=event_name):
        if event.respo_sign and event.president_sign and \
            (event.tutor_sign or not event.alcoholic_drinks)\
            and person == Administration.objects.get(role="Respo asso").person_id:

                for ev2 in Event.objects.filter(date=event.date):
                    if ev2 != event \
                        and ev2.hour_begin <= event.end \
                        and ev2.hour_end >= event.hour_begin:

                        remove_conflict_event(event, ev2)

                return HttpResponseRedirect('/thanks')

    raise Http404("You can't pass this event to premium")


# form create asso
@login_required
def create_asso(request):
    error = ""

    users = []
    for p in Person.objects.all():
        users.append(p.name)

    if request.method == 'POST':
        form = forms.AssoCreateForm(request.POST, request.FILES)
        if form.is_valid():
            # put the new person in the database
            asso_name = form.cleaned_data['name']
            asso_description = form.cleaned_data['description']
            asso_president = form.cleaned_data['president']
            asso_tresorier = form.cleaned_data['tresorier']
            asso_secretaire = form.cleaned_data['secretaire']

            error = "Asso already exists"

            if Association.objects.filter(name=asso_name).count() != 0:
                context = { 'form': forms.AssoCreateForm(),
                            'users': users,
                            'error': error,
                        }
                return render(request, 'association_create.html', context)

            asso = Association(name=asso_name, image=request.FILES['image'], creation_date=datetime.now(), description=asso_description)
            asso.save()

            error = 'Bureau members not found'

            if Person.objects.filter(name=asso_president).count() == 1 \
                and Person.objects.filter(name=asso_tresorier).count() == 1 \
                and Person.objects.filter(name=asso_secretaire).count() == 1:

                asso = Association.objects.filter(name=asso_name)[0]

                asso_pres = Person.objects.get(name=asso_president)
                asso_tres = Person.objects.get(name=asso_tresorier)
                asso_secr = Person.objects.get(name=asso_secretaire)

                bureau1 = Bureau(association_id=asso, person_id=asso_pres, role="President")
                bureau2 = Bureau(association_id=asso, person_id=asso_tres, role="Tresorier")
                bureau3 = Bureau(association_id=asso, person_id=asso_secr, role="Secretaire")

                bureau1.save()
                bureau2.save()
                bureau3.save()

                memb1 = Member(association_id=asso, person_id=asso_pres)
                memb1.save()
                if asso_pres != asso_tres:
                    memb2 = Member(association_id=asso, person_id=asso_tres)
                    memb2.save()

                    if asso_tres == asso_secr:
                        return HttpResponseRedirect('/thanks')

                if asso_pres != asso_secr:
                    memb3 = Member(association_id=asso, person_id=asso_secr)
                    memb3.save()

            # redirect
            return HttpResponseRedirect('/thanks')
    else:
        form = forms.AssoCreateForm()

    person = Person.objects.get(name=request.user.username)
    context = { 'form': form,
                'users': users,
                'is_administration': Administration.objects.filter(person_id=person).count(),
                'error': error,
                'user': person,
                }
    return render(request, 'association_create.html', context)

@login_required
def del_asso(request, asso_name):
    if Association.objects.filter(name=asso_name).count() != 1:
        raise Http404("association not found")

    asso = Association.objects.get(name=asso_name)
    # remove the image from folders
    os.remove(asso.image.path)

    asso.delete()

    return HttpResponseRedirect('/')







@login_required
def logged(request):
    context = {

        'user': request.user,
        'extra_data': request.user.social_auth.get(provider="epita").extra_data,
    }
    if Person.objects.filter(name=request.user.username).count() > 0: # already registered
        person = Person.objects.get(name=request.user.username)
        if not person.passphrase:
            return HttpResponseRedirect("/passphrase/", request)
        return HttpResponseRedirect('/accueil')

    # put the new person in the database
    p_name = request.user.username
    p_date = datetime.now()
    p_telephone = context["extra_data"]["phone_number"]
    p_administration = context["extra_data"]["promo"] == "adm"
    p_email = request.user.email
    p = Person(name=p_name,
               inscription_date=p_date,
               administration=p_administration,
               telephone=p_telephone,
               email=p_email,
               signature_public_key="")
    p.save()
    #redirect
    return HttpResponseRedirect("/passphrase/", request)

def passphrase(request):
    if request.method == 'POST':
        form = forms.PassphraseForm(request.POST)
        if form.is_valid():
            #generate private key and send it to trusted third party
            pphrase = form.cleaned_data['passphrase']

            gpg = gnupg.GPG()
            input_data = gpg.gen_key_input(key_type="RSA", key_length=1024,
                                           name_email=request.user.email,
                                           name_real=request.user.username,
                                           passphrase=pphrase)
            key = gpg.gen_key(input_data)

            ascii_armored_private_keys = gpg.export_keys(
                keyids=key.fingerprint,
                secret=True,
                passphrase=pphrase
            )
            ascii_armored_public_keys = gpg.export_keys(key.fingerprint, passphrase=pphrase)
            print("private key:\n" + ascii_armored_private_keys)
            print("public key:\n" + ascii_armored_public_keys)

            with open("events/static/keys/" + request.user.username, 'w') as key_file:
                key_file.write(ascii_armored_private_keys)

            r = requests.post(THIRD_PARTY_ADDRESS + "/set_public_key/" + request.user.username,
                             ascii_armored_public_keys)

            print(r)

            p = Person.objects.get(name=request.user.username)
            p.passphrase = True
            p.save()
            return HttpResponseRedirect('/signature/')
    else:
        form = forms.PassphraseForm()
    context = {
        'user': request.user,
        'form': form
    }
    return render(request, 'connection/passphrase.html', context)


# form of inscription
@login_required
def signature_missing(request):
    if request.method == 'POST':
        form = forms.PersonForm(request.POST, request.FILES)
        if form.is_valid():
            p = Person.objects.get(name=request.user.username)
            p.image = request.FILES['image']
            p.save()
            return HttpResponseRedirect('/thanks')
    else:
        form = forms.PersonForm()
    context = {
        'user': request.user,
        'form': form
    }
    return render(request, 'connection/signature.html', context)












def autoCompletion(request):
    l = []
    for e in Person.objects.all():
        l.append(e.name)

    context = {
        'persons': l
    }
    return render(request, 'testing_autocompletion2.html', context)



@login_required
def AddAssoEvent(request, event_name):
    if Event.objects.filter(name=event_name).count() != 1:
        raise Http404("event not found")

    event = Event.objects.get(name=event_name)

    if request.method == 'POST':
        form = forms.EventAddAssoForm(request.POST, request.FILES)
        if form.is_valid():
            asso_names = request.POST.getlist('assos')

            before = []

            for e in AssoSupEvent.objects.filter(event_id=event):
                before.append(e.association_id)
                e.delete()

            for name in asso_names:
                asso = Association.objects.get(name=name)

                a = AssoSupEvent(event_id=event, association_id=asso)
                a.save()

                if not asso in before:
                    try:
                        president = Bureau.objects.get(association_id=asso, role="President").person_id
                        send_email_to([president.email], 3)
                    except:
                        print("error sending mail")
                else:
                    before.remove(asso)

            for asso in before:
                try:
                    president = Bureau.objects.get(association_id=asso, role="President").person_id
                    send_email_to([president.email], 4)
                except:
                    print("error sending mail")

            # redirect
            return HttpResponseRedirect('/thanks')

    else:
        form = forms.EventAddAssoForm()

    asso_can_add = []
    for a in Association.objects.all():
        if a != event.association_id:
            if AssoSupEvent.objects.filter(event_id=event, association_id=a).count() > 0:
                asso_can_add.append((a.name, True))
            else:
                asso_can_add.append((a.name, False))

    person = Person.objects.get(name=request.user.username)
    context =  { 'form': form,
                 'event': event,
                 'asso_list': asso_can_add,
                 'is_administration': Administration.objects.filter(person_id=person).count(),
                 'user': person,
            }
    return render(request, 'add_asso_to_event.html', context)






@login_required
def AddMember(request, asso_name):
    if Association.objects.filter(name=asso_name).count() != 1:
        raise Http404("association not found")

    asso = Association.objects.get(name=asso_name)

    if request.method == 'POST':
        form = forms.AssoAddMemberForm(request.POST, request.FILES)
        if form.is_valid():
            person_name = form.cleaned_data['member']

            if Person.objects.filter(name=person_name).count() == 1:
                person = Person.objects.get(name=person_name)

                member = Member(association_id=asso, person_id=person)
                member.save()

                # redirect
                return HttpResponseRedirect('/thanks')

    else:
        form = forms.AssoAddMemberForm()

    person = Person.objects.get(name=request.user.username)
    context =  { 'form': form,
                 'asso': asso,
                 'is_administration': Administration.objects.filter(person_id=person).count(),
                 'user': person,
            }
    return render(request, 'member_add.html', context)


@login_required
def DelMember(request, asso_name):
    if Association.objects.filter(name=asso_name).count() != 1:
        raise Http404("association not found")

    asso = Association.objects.get(name=asso_name)

    if request.method == 'POST':
        form = forms.AssoDelMemberForm(request.POST, request.FILES)
        if form.is_valid():
            person_name = form.cleaned_data['member']

            if Person.objects.filter(name=person_name).count() == 1:
                person = Person.objects.get(name=person_name)

                if Member.objects.filter(association_id=asso, person_id=person).count() == 1:
                    Member.objects.get(association_id=asso, person_id=person).delete()

                    # redirect
                    return HttpResponseRedirect('/thanks')

    else:
        form = forms.AssoDelMemberForm()

    person = Person.objects.get(name=request.user.username)
    context = { 'form': form,
                'asso': asso,
                'is_administration': Administration.objects.filter(person_id=person).count(),
                'user': person,
            }
    return render(request, 'member_del.html', context)


import io
from django.http import FileResponse
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch
from reportlab.lib.pagesizes import A4
from reportlab.platypus import Image

def create_pdf(event_name):

    if Event.objects.filter(name=event_name).count() != 1:
        return

    event = Event.objects.get(name=event_name)
    association = event.association_id
    creator = event.creator_id
    respo = event.respo_id
    tutor = None
    if event.tutor_id:
        tutor = event.tutor_id.person_id

    # create a canvas (a pdf)
    p = canvas.Canvas("events/static/pdf/fiche-event-" + str(event.id)
        + "-" + event.name  + ".pdf")

    # margin of the page
    top_margin = A4[1] - inch
    bottom_margin = inch
    left_margin = inch
    right_margin = A4[0] - inch
    frame_width = right_margin - left_margin

    # draw a line
    #p.line(left_margin, top_margin, right_margin, top_margin)

    #p.setFont('Times-Italic', 12)

    # add text
    #p.drawString(left_margin, top_margin + 2, "Hello world")
    # draw 2 lines
    #p.line(left_margin, top_margin, right_margin, top_margin)
    #p.line(left_margin, bottom_margin, right_margin, bottom_margin)

    p.setFont("Times-Bold", 30)
    # add text at center (first arg = padding left, 2nd arg = padding down)
    p.drawCentredString(0.5 * A4[0], 10.5 * inch, "Fiche Event " + str(event.id))

    p.setFont("Times-Bold", 12)

    # create a paragraph
    tx = p.beginText(left_margin, 9.5 * inch)
    # add line to paragraphe
    tx.textLine("organisé par l'association : " + association.name)

    tx.textLine("Nom de l'Event : " + event.name)

    tx.textLine("Date de l'Event : " + str(event.date))
    tx.textLine("Horaire de début : " + str(event.hour_begin))
    tx.textLine("Horaire de fin : " + str(event.hour_end))


    tx.textLine("Recurrence :")
    if (event.recurrence):
        tx.textLine("oui")
        tx.textLine("Frequence : " + str(event.frequence))
        tx.textLine("Fin : " + str(event.frequence_end))
    else:
        tx.textLine("non")

    tx.textLine("Responsable sur place : " + respo.name)
    tx.textLine("Tel du responsable sur place : " + respo.telephone)
    tx.textLine("Classe du responsable sur place : " + event.respo_class)

    if event.alcoholic_drinks and tutor:
        tx.textLine("Tuteur : " + tutor.name)
        tx.textLine("Tel du tuteur : " + tutor.telephone)
        tx.textLine("Poste du tuteur : " + event.tutor_id.role)

    tx.textLine("Nombre d'externes attendus : " + str(event.nb_externs))
    tx.textLine("Nombre d'etudiants IONIS attendus : " + str(event.nb_students))
    tx.textLine("Nombre de membres de l'association : " + str(event.nb_members))

    tx.textLine("Description de l'event :")
    tx.textLine(event.description)

    tx.textLine("Assurance :")
    if (event.insurance):
        tx.textLine("oui")
    else:
        tx.textLine("non")

    tx.textLine("Materiel : " + event.material)

    tx.textLine("Boissons alcoolisees :")
    if (event.alcoholic_drinks):
        tx.textLine("oui")
    else:
        tx.textLine("non")

    site = "KB"
    for room in ReservedRoom.objects.filter(event_id=event):
        if room.room_id.KB_VJ:
            site = "VJ"
        break

    tx.textLine("Site : " + site)
    tx.textLine("Salles prévues :")
    rs = ""
    k = 0
    # return carriage if > 85 characters
    for room in ReservedRoom.objects.filter(event_id=event):
        if k != 0:
            rs += ", "
        if k > 85:
            tx.textLine(rs)
            rs = ""
            k = 0

        rs += room.room_id.name
        k += len(room.room_id.name) + 2

    tx.textLine(rs)

    tx.textLine("Materiel présent:")
    tx.textLine(event.material)

    tx.textLine("Boissons prévues :")
    if not event.soft_drinks and not event.alcoholic_drinks:
        tx.textLine("Aucune boisson")
    elif event.soft_drinks:
        tx.textLine("Boissons non alcoolisées")
    else:
        tx.textLine("Boissons non alcoolisées et alcoolisées")

    tx.textLine("Commentaires : " + event.comments)

    tx.textLine("Associations invitées:")
    asso = ""
    k = 0
    for a in AssoSupEvent.objects.filter(event_id=event):
        if k != 0:
            asso += ", "
        if k > 85:
            tx.textLine(asso)
            asso = ""
            k = 0

        asso += a.association_id.name
        k += len(a.association_id.name) + 2
    tx.textLine(asso)
    tx.textLine()

    tx.textLine("Validation de l'event :")
    if (event.respo_sign):
        tx.textLine("Responsable de l'event : oui")
    else:
        tx.textLine("Responsable de l'event : non")
    if (event.president_sign):
        tx.textLine("President.e de l'association : oui")
    else:
        tx.textLine("President.e de l'association : non")
    if event.tutor_sign and event.alcoholic_drinks:
        tx.textLine("Tuteur de l'event : oui")
    elif (event.alcoholic_drinks):
        tx.textLine("Tuteur de l'event : non")
    if (event.respo_asso_sign):
        tx.textLine("Responsable associatif : oui")
    else:
        tx.textLine("Responsable associatif : non")

    # put paragraph to page
    p.drawText(tx)

    # rectangles and lines : design of pdf
    p.rect(60, 580, 480, 100)
    p.line(60, 665, 540, 665)

    # respo asso
    p.drawString(right_margin - inch, 2.2 * inch, "Responsable associations")
    if event.respo_asso_sign:
        image = Image('./events/static/' + Administration.objects.get(role="Respo asso").person_id.getPath())
        image._restrictSize(2 * inch, 1.5 * inch)
        image.drawOn(p, right_margin - inch, 0.5 * inch)
    # tutor
    p.drawString(right_margin - 2.5 * inch, 2.2 * inch, "Tuteur")
    if event.tutor_sign and event.alcoholic_drinks:
        image = Image('./events/static/' + event.tutor_id.person_id.getPath())
        image._restrictSize(2 * inch, 1.5 * inch)
        image.drawOn(p, right_margin - 3 * inch, 0.5 * inch)
    # president of asso
    p.drawString(right_margin - 5 * inch, 2.2 * inch, "Président de l'association")
    if event.president_sign:
        image = Image('./events/static/' + Bureau.objects.get(association_id=event.association_id, role="President").person_id.getPath())
        image._restrictSize(2 * inch, 1.5 * inch)
        image.drawOn(p, right_margin - 5 * inch, 0.5 * inch)
    # respo of event
    p.drawString(right_margin - 7 * inch, 2.2 * inch, "Responsable sur place")
    if event.respo_sign:
        image = Image('./events/static/' + event.respo_id.getPath())
        image._restrictSize(2 * inch, 1.5 * inch)
        image.drawOn(p, right_margin - 7 * inch, 0.5 * inch)

    # save the pdf
    p.showPage()
    p.save()

    return HttpResponseRedirect("/list_pdf")



@login_required
def list_pdf(request):
    person = Person.objects.get(name=request.user.username)
    if Administration.objects.filter(person_id=person).count() == 0 :
        return not_authorized(request)

    l = []

    for event in Event.objects.all():
        create_pdf(event.name)
        l.append("fiche-event-" + str(event.id) + "-" + event.name + '.pdf')

    context = { 'list': l,
                'user': person,
            }

    return render(request, 'list_pdf.html', context)

@login_required
def show_pdf(request, pdf_name):
    try:
        create_pdf(pdf_name)
        path = "events/static/pdf/" + pdf_name

        response = FileResponse(open(path, 'rb'), content_type='application/pdf')
        response['Content-Disposition'] = 'inline; filename=' + pdf_name + '.pdf'
        return response

    except:
        raise Http404("pdf not found")

@login_required
def show_pdf_updated(request, event_name):
    try:
        create_pdf(event_name)
        if Event.objects.filter(name=event_name).count() != 1:
            return

        event = Event.objects.get(name=event_name)
        path = "events/static/pdf/fiche-event-" + str(event.id) + "-" + event.name + '.pdf'

        response = FileResponse(open(path, 'rb'), content_type='application/pdf')
        response['Content-Disposition'] = 'inline; filename=' + event_name + '.pdf'
        return response

    except:
        raise Http404("pdf not found")


from django.core.mail import send_mail
from django.conf import settings

def send_email_to(mail, message_num):
    email_from = 'Fiche Event web site' #'fiche.event.amelie.bertin@gmail.com'
    recipient_list = mail
    subject = ""
    message = ""
    if message_num == 0:
        subject = 'Fiche event envoyée'
        message = 'Bonjour,\n\nVotre fiche évent a bien été enregistrée, elle va être envoyée aux différents signataires pour vérifier son contenu.\n\nCordialement,\n\nL\'équipe Team Fiche Event d\'Amelie Bertin, Claire Billy, Valentin Henry, Moemoea Fierin.'
    elif message_num == 1:
        subject = 'Fiche évent: demande de confirmation'
        message = 'Bonjour,\n\nUne fiche évent vient d\'être deposée, nous vous demandons de vous connecter sur le site pour la vérifier et valider.\n\nMerci,\nCordialement,\n\nL\'équipe Team Fiche Event d\'Amelie Bertin, Claire Billy, Valentin Henry, Moemoea Fierin.'
    elif message_num == 2:
        subject = 'Fiche évent: confirmation'
        message = 'Bonjour,\n\nVous venez de valider une fiche évent.\n\nCordialement,\n\nL\'équipe Team Fiche Event d\'Amelie Bertin, Claire Billy, Valentin Henry, Moemoea Fierin.'
    elif message_num == 3:
        subject = 'Fiche évent: ajout'
        message = 'Bonjour,\n\nVotre association vient d\'être ajoutée à une fiche évent.\nNous vous conseillons de vérifier votre calendrier.\n\nCordialement,\n\nL\'équipe Team Fiche Event d\'Amelie Bertin, Claire Billy, Valentin Henry, Moemoea Fierin.'
    elif message_num == 4:
        subject = 'Fiche évent: retiré'
        message = 'Bonjour,\n\nVotre association vient d\'être retirée d\'une fiche évent.\nNous vous conseillons de vérifier votre calendrier.\n\nCordialement,\n\nL\'équipe Team Fiche Event d\'Amelie Bertin, Claire Billy, Valentin Henry, Moemoea Fierin.'
    elif message_num == -1:
        subject = 'Fiche évent: erreur'
        message = 'Bonjour,\n\nUne fiche évent que vous avez déposée vient d\'être rejetée, nous vous demandons de vous connecter sur le site pour vérifier et en déposer une nouvelle si besoin.\n\nCordialement,\n\nL\'équipe Team Fiche Event d\'Amelie Bertin, Claire Billy, Valentin Henry, Moemoea Fierin.'
    elif message_num == -2:
        subject = 'Fiche évent: erreur'
        message = 'Bonjour,\n\nUne fiche évent que vous avez déposée contient des salles réservées par d\'autres événements, nous vous demandons de vous connecter sur le site pour vérifier et en déposer une nouvelle si besoin.\n\nCordialement,\n\nL\'équipe Team Fiche Event d\'Amelie Bertin, Claire Billy, Valentin Henry, Moemoea Fierin.'
    else:
        subject = 'Fiche évent: confirmée'
        message = 'Bonjour,\n\nUne fiche évent que vous avez déposée a été validée par tous les signataires. Vous pouvez vous connecter sur le site pour plus d\'informations.\n\nCordialement,\n\nL\'équipe Team Fiche Event d\'Amelie Bertin, Claire Billy, Valentin Henry, Moemoea Fierin.'
    send_mail(subject, message, email_from, recipient_list, fail_silently=False)


@login_required
def emails(request):
    send_email_to(['amelie.bertin@epita.fr'], 0)
    send_email_to(['amelie.bertin@epita.fr'], 1)
    send_email_to(['amelie.bertin@epita.fr'], 2)
    send_email_to(['amelie.bertin@epita.fr'], -1)
    send_email_to(['amelie.bertin@epita.fr'], 3)
    return redirect('/')

@login_required
def get_stats(request):
    person = Person.objects.get(name=request.user.username)
    if Administration.objects.filter(person_id=person).count() == 0 :
        return not_authorized(request)
    nb_person = Person.objects.all().count()
    nb_adm = Person.objects.filter(administration=True).count()
    nb_student = nb_person - nb_adm
    nb_association = Association.objects.all().count()
    members = []
    for asso in Association.objects.all():
        members.append((asso.name, asso.members.count()))
    events = []
    for event in Event.objects.all():
        i = "Non signée"
        if event.respo_sign:
            i = "Signée uniquement par le responsable sur place"
            if event.president_sign:
                i = "Signée par le responsable sur place et par le président de l'association"
                if event.tutor_sign:
                    i = "Signée par le responsable sur place, le président de l'association et le tuteur"
                    if event.respo_asso_sign:
                        i = "Event validé par l'administration"
        events.append((event.name, event.date, i, event.association_id.name))

    context = {
        'nb_person': nb_person,
        'nb_adm': nb_adm,
        'nb_student': nb_student,
        'nb_association': nb_association,
        'members': members,
        'events': events,
        'user': person,
        'is_administration': Administration.objects.filter(person_id=person).count() > 0,
    }
    return render(request, 'new_stats.html', context)


from django.contrib import auth
@login_required
def logout(request):
  auth.logout(request)
  # Redirect to a success page.
  return HttpResponseRedirect("/")
