from django.urls import path
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('accueil', views.accueil, name='accueil'), #changed

    path('', views.index, name='index'),
    path('form', views.get_name),
    path('thanks', views.thanks),
    path('profils', views.profils_view, name='profils_list'),

    path('asso', views.AssosView, name='asso'),
    path('create_asso', views.create_asso),
    path('asso_del/<str:asso_name>', views.del_asso),
    path('asso/<name>', views.AssoDetail, name='asso_detail'),

    path('events', views.EventsView, name='events'),
    path('form_event', views.get_event),

    path('profils/<str:name_>', views.profil_detail, name='profil_detail'),
    path('logged/', views.logged, name='logged'),
    path('passphrase/', views.passphrase, name='passphrase'),
    path('signature/', views.signature_missing),

    path('autocomp/', views.autoCompletion),
    path('member_add/<str:asso_name>', views.AddMember),
    path('member_del/<str:asso_name>', views.DelMember),

    path('pdf/<str:event_name>', views.show_pdf_updated),
    path('list_pdf/', views.list_pdf),
    path('show_pdf/<str:pdf_name>', views.show_pdf),

    path('mail/', views.emails),

    path('sign/<str:event_name>', views.sign),
    path('reject/<str:event_name>', views.reject),
    path('premium/<str:event_name>', views.premium),
    path('add_asso_event/<str:event_name>', views.AddAssoEvent),

    path('stats/', views.get_stats),

    path('logout/', views.logout, name='logout')
]

urlpatterns += staticfiles_urlpatterns()
