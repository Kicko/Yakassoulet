from django import forms
from . import models


class NameForm(forms.Form):
    your_name = forms.CharField(label='Your name', max_length=100)


class PersonForm(forms.ModelForm):
    class Meta:
        model = models.Person
        image = forms.ImageField()
        fields = []


class PassphraseForm(forms.Form):
    passphrase = forms.CharField(label='passphrase', max_length=512)


class EventForm(forms.ModelForm):
    association = forms.CharField(max_length=255, required=False,
        widget = forms.TextInput(attrs = {
            'id': 'asso',
            'class': 'search',
            'autocomplete': 'off',
        })
    )
    respo_name = forms.CharField(max_length=255, required=False,
        widget = forms.TextInput(attrs = {
            'id': 'respo',
            'class': 'search',
            'autocomplete': 'off',
        })
    )
    tutor_name = forms.CharField(max_length=255, required=False,
        widget = forms.TextInput(attrs = {
            'id': 'tutor',
            'class': 'search',
            'autocomplete': 'off',
        })
    )
    #rooms = forms.CharField(max_length=511)
    #site = forms.CharField(max_length=100)

    class Meta:
        model = models.Event
        fields = [
            'name',
            'date',
            'hour_begin',
            'hour_end',
            'recurrence',
            'frequence',
            'frequence_end',
            'respo_class',
            'nb_students',
            'nb_members',
            'nb_externs',
            'description',
            'insurance',
            'material',
            'alcoholic_drinks',
            'soft_drinks',
            'comments'
        ]
        widgets = {'respo_id': forms.TextInput}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field in self.Meta.fields:
            self.fields[field].required = False

class AssoCreateForm(forms.ModelForm):
    president = forms.CharField(max_length=255,
        widget = forms.TextInput(attrs = {
            'id': 'pres',
            'class': 'search',
            'autocomplete': 'off',
        })
    )
    tresorier = forms.CharField(max_length=255,
        widget = forms.TextInput(attrs = {
            'id': 'tres',
            'class': 'search',
            'autocomplete': 'off',
        })
    )
    secretaire = forms.CharField(max_length=255,
        widget = forms.TextInput(attrs = {
            'id': 'secr',
            'class': 'search',
            'autocomplete': 'off',
        })
    )
    description = forms.CharField(widget=forms.Textarea)
    class Meta:
        model = models.Association
        image = forms.ImageField()
        fields = ['name']

class AssoAddMemberForm(forms.ModelForm):
    member = forms.CharField(max_length=255)
    class Meta:
        model = models.Member
        fields = []

class AssoDelMemberForm(forms.ModelForm):
    member = forms.CharField(max_length=255)
    class Meta:
        model = models.Member
        fields = []

class EventAddAssoForm(forms.ModelForm):
    class Meta:
        model = models.AssoSupEvent
        fields = []
