from django.db import models
from datetime import datetime
from django.utils import timezone

# Create your models here.

class Person(models.Model):
    name = models.CharField(max_length=255)
    inscription_date = models.DateTimeField('inscription date')
    administration = models.BooleanField(default=False)
    telephone = models.CharField(max_length=255)
    email = models.CharField(max_length=255, default="")
    passphrase = models.BooleanField(default=False)
    image = models.ImageField(upload_to='events/static/images/', null=True)

    def __str__(self):
        return self.name

    def getPath(self):
        l = self.image.path.split('/')
        index = len(l) - 2
        return "/".join(l[index:])

class Association(models.Model):
    name = models.CharField(max_length=255)
    creation_date = models.DateTimeField(auto_now=True)
    description = models.TextField(default="")
    image = models.ImageField(upload_to='static/uploaded-files/')

    def __str__(self):
        return self.name

    def getPath(self):
        l = self.image.path.split('/')
        index = len(l) - 2
        return "/".join(l[index:])

class Bureau(models.Model):
    association_id = models.ForeignKey(Association, on_delete=models.CASCADE, related_name='bureau')
    person_id = models.ForeignKey(Person, on_delete=models.CASCADE)
    role = models.CharField(max_length=255)

    def __str__(self):
        return self.role + " of association " + str(self.association_id)

class Member(models.Model):
    association_id = models.ForeignKey(Association, on_delete=models.CASCADE, related_name='members')
    person_id = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='memb')

    def __str__(self):
        return "member of " + str(self.association_id)

class Administration(models.Model):
    person_id = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='adm')
    role = models.CharField(max_length=255)

    def __str__(self):
        return "adm : " + self.role

class Event(models.Model):
    association_id = models.ForeignKey(Association, on_delete=models.CASCADE, related_name='events')
    creator_id = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='event')
    name = models.CharField(max_length=255)
    date = models.DateField(default=timezone.now())
    hour_begin = models.TimeField(default=datetime.strptime('00:00', '%H:%M').time())
    hour_end = models.TimeField(default=datetime.strptime('23:59', '%H:%M').time())
    recurrence = models.BooleanField(default=False)
    frequence = models.IntegerField(blank=True, null=True)
    frequence_end = models.DateField(blank=True, null=True)
    respo_id = models.ForeignKey(Person, on_delete=models.DO_NOTHING, related_name='respo_event')
    respo_class = models.CharField(max_length=255)
    tutor_id = models.ForeignKey(Administration, on_delete=models.DO_NOTHING, related_name='tutor_event', null=True)
    nb_externs = models.IntegerField()
    nb_students = models.IntegerField()
    nb_members = models.IntegerField()
    description = models.CharField(max_length=1000)
    insurance = models.BooleanField(default=False)
    material = models.CharField(max_length=1000, blank=True)
    alcoholic_drinks = models.BooleanField(default=False)
    soft_drinks = models.BooleanField(default=False)
    comments = models.CharField(max_length=1000, blank=True)
    respo_sign = models.BooleanField(default=False)
    president_sign = models.BooleanField(default=False)
    tutor_sign = models.BooleanField(default=False)
    respo_asso_sign = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def get_day(self):
        return self.date.day

    def get_month(self):
        if self.date.month == 1:
            return 'JANV'
        elif self.date.month == 2:
            return 'FEVR'
        elif self.date.month == 3:
            return 'MARS'
        elif self.date.month == 4:
            return 'AVR'
        elif self.date.month == 5:
            return 'MAI'
        elif self.date.month == 6:
            return 'JUIN'
        elif self.date.month == 7:
            return 'JUIL'
        elif self.date.month == 8:
            return 'AOUT'
        elif self.date.month == 9:
            return 'SEPT'
        elif self.date.month == 10:
            return 'OCT'
        elif self.date.month == 11:
            return 'NOV'
        else:
            return 'DEC'

    def get_begin_hour(self):
        return self.hour_begin.strftime("%H:%M")
    def get_end_hour(self):
        return self.hour_end.strftime("%H:%M")

class PossibleRoom(models.Model):
    name = models.CharField(max_length=100)
    KB_VJ = models.BooleanField(default=False) # KB = false, VJ = true

    def __str__(self):
        return self.name

class ReservedRoom(models.Model):
    event_id = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='rooms')
    room_id = models.ForeignKey(PossibleRoom, on_delete=models.CASCADE, related_name='reservations')

    def __str__(self):
        return "room " + str(self.room_id) + " reserved by " + str(self.event_id)

class Brouillon(models.Model):
    association = models.CharField(max_length=1000)
    creator_id = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='brouillon')
    creation_date = models.DateTimeField('creation date', null=True)
    name = models.CharField(max_length=1000)
    date = models.CharField(max_length=1000)
    hour_begin = models.CharField(max_length=1000)
    hour_end = models.CharField(max_length=1000)
    recurrence = models.BooleanField(default=False)
    frequence = models.CharField(max_length=1000, null=True)
    frequence_end = models.CharField(max_length=1000, null=True)
    respo_id = models.CharField(max_length=1000, null=True)
    respo_class = models.CharField(max_length=1000)
    tutor_id = models.CharField(max_length=1000)
    nb_externs = models.CharField(max_length=1000)
    nb_students = models.CharField(max_length=1000)
    nb_members = models.CharField(max_length=1000)
    description = models.CharField(max_length=1000)
    insurance = models.BooleanField(default=False)
    material = models.CharField(max_length=1000)
    alcoholic_drinks = models.BooleanField(default=False)
    soft_drinks = models.BooleanField(default=False)
    comments = models.CharField(max_length=1000)
    rooms_VJ = models.CharField(max_length=1000, null=True)
    rooms_KB = models.CharField(max_length=1000, null=True)

class AssoSupEvent(models.Model):
    event_id = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='asso_sup')
    association_id = models.ForeignKey(Association, on_delete=models.CASCADE, related_name='events_sup')

    def __str__(self):
        return "asso " + self.association_id.name + " est ajoute a l'event " + self.event_id.name
