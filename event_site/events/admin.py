from django.contrib import admin

# Register your models here.
from . import models

@admin.register(models.Association)
class AssociationAdmin(admin.ModelAdmin):
    pass

@admin.register(models.Person)
class PersonAdmin(admin.ModelAdmin):
    pass

@admin.register(models.Bureau)
class BureauAdmin(admin.ModelAdmin):
    pass

@admin.register(models.Member)
class MemberAdmin(admin.ModelAdmin):
    pass

@admin.register(models.Event)
class EventAdmin(admin.ModelAdmin):
    pass

@admin.register(models.PossibleRoom)
class PossibleRoomAdmin(admin.ModelAdmin):
    pass

@admin.register(models.ReservedRoom)
class ReservedRoomAdmin(admin.ModelAdmin):
    pass

@admin.register(models.Administration)
class AdministrationAdmin(admin.ModelAdmin):
    pass

@admin.register(models.Brouillon)
class BrouillonAdmin(admin.ModelAdmin):
    pass

@admin.register(models.AssoSupEvent)
class AssoSupEventAdmin(admin.ModelAdmin):
    pass
