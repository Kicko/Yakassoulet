from django.test import TestCase

# Create your tests here.
class TestLinks(TestCase):
    def test_exists(self):
        self.assertEqual(self.client.get('').status_code, 200)

    def test_links_to_events(self):
        response = self.client.get('')
        self.assertContains(response, 'href="/events"')

    def test_links_to_asso(self):
        response = self.client.get('')
        self.assertContains(response, 'href="/asso"')

    def test_links_to_profils(self):
        response = self.client.get('')
        self.assertContains(response, 'href="/profils"')

    def test_links_to_connexion(self):
        response = self.client.get('')
        self.assertContains(response, 'href="/auth/login/') #connection
